FROM diouxx/glpi:latest	
MAINTAINER Marcelo Motta "marcelo.motta@mxserv.com.br"

#Copia o entrypoint
COPY glpi-start.sh /opt/
RUN chmod +x /opt/glpi-start.sh
ENTRYPOINT ["/opt/glpi-start.sh"]

#Expoe as portas
EXPOSE 80 443

